# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import get_object_or_404
from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
from datetime import date
# Create your models here.
	
User_Type = (
	('ADMIN', 'Administrador'),
	('TRANS', 'Transportista'),
	)
User_Type_Default='Administrador'

@python_2_unicode_compatible
class Perfiles_de_usuario(models.Model):
	Usuario = models.OneToOneField(User, on_delete=models.CASCADE)
	Tipo_de_perfil = models.CharField(
		max_length=10,
		choices= User_Type,
		default=User_Type_Default)
	Empresa = models.CharField(max_length=50, null=True, blank=True)
	Rut = models.CharField(max_length=20)
	def __str__(self):
		return "%s" % (self.Usuario)

@python_2_unicode_compatible
class Vehiculo(models.Model):
	Patente = models.CharField(max_length=8, primary_key=True)
	Perfil = models.ForeignKey('Perfiles_de_usuario')
	Marca = models.CharField(max_length=20, null=True)
	Modelo = models.CharField(max_length=20, null=True)
	Tipo_vehiculo = models.CharField(max_length=10, null=True)
	Color_vehiculo = models.CharField(max_length=20 , null= True, blank=True)
	def __str__(self):
		return "%s" % (self.Patente)

@python_2_unicode_compatible
class Entrega(models.Model):
	Nro_documento = models.CharField(max_length=50,null=True,blank=True)
	Tipo_documento = models.CharField(max_length=20, null=True, blank=True)
	Orden_compra = models.CharField(max_length=50,null=True,blank=True)
	Vehiculo= models.ForeignKey('Vehiculo', blank = True, null = True)
	Transportista = models.CharField(max_length=50,null=True,blank=True)
	Destino = models.CharField(max_length=50,null=True,blank=True)
	Direccion_destino = models.CharField(max_length=100,null=True,blank=True)
	Lat_cliente = models.CharField(max_length=150,null=True,blank=True)
	Lng_cliente = models.CharField(max_length=150, null=True,blank=True)
	Cantidad_bultos = models.IntegerField(null=True, blank=True)
	Cantidad_pallets = models.IntegerField(null=True, blank=True)
	Tipo_entrega = models.CharField(max_length=50, null=True, blank=True)
	Motivo = models.CharField(max_length=30, null=True,blank=True)
	Status_entrega = models.CharField(max_length=15, null=True, blank=True)
	def __str__(self):
		return "%s - %s - %s - %s" % (self.Vehiculo, self.Nro_documento, self.Orden_compra, self.Destino)

class Posicion(models.Model):
	Vehiculo= models.OneToOneField('Vehiculo', blank = True, null = True)
	Latitud = models.CharField(max_length=20)
	Longitud = models.CharField(max_length=20)
	Status = models.BooleanField(default=False)
