# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-02-19 18:28
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Entrega',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nro_documento', models.CharField(blank=True, max_length=50, null=True)),
                ('Tipo_documento', models.CharField(blank=True, max_length=20, null=True)),
                ('Orden_compra', models.CharField(blank=True, max_length=50, null=True)),
                ('Transportista', models.CharField(blank=True, max_length=50, null=True)),
                ('Destino', models.CharField(blank=True, max_length=50, null=True)),
                ('Direccion_destino', models.CharField(blank=True, max_length=100, null=True)),
                ('Lat_cliente', models.CharField(blank=True, max_length=150, null=True)),
                ('Lng_cliente', models.CharField(blank=True, max_length=150, null=True)),
                ('Cantidad_bultos', models.IntegerField(blank=True, null=True)),
                ('Cantidad_pallets', models.IntegerField(blank=True, null=True)),
                ('Tipo_entrega', models.CharField(blank=True, max_length=50, null=True)),
                ('Motivo', models.CharField(blank=True, max_length=30, null=True)),
                ('Status_entrega', models.CharField(blank=True, max_length=15, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Perfiles_de_usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Tipo_de_perfil', models.CharField(choices=[('ADMIN', 'Administrador'), ('TRANS', 'Transportista')], default='Administrador', max_length=10)),
                ('Empresa', models.CharField(blank=True, max_length=50, null=True)),
                ('Rut', models.CharField(max_length=20)),
                ('Usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Posicion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Latitud', models.CharField(max_length=20)),
                ('Longitud', models.CharField(max_length=20)),
                ('Status', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Vehiculo',
            fields=[
                ('Patente', models.CharField(max_length=8, primary_key=True, serialize=False)),
                ('Marca', models.CharField(max_length=20, null=True)),
                ('Modelo', models.CharField(max_length=20, null=True)),
                ('Tipo_vehiculo', models.CharField(max_length=10, null=True)),
                ('Color_vehiculo', models.CharField(blank=True, max_length=20, null=True)),
                ('Perfil', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Perfiles_de_usuario')),
            ],
        ),
        migrations.AddField(
            model_name='posicion',
            name='Vehiculo',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Vehiculo'),
        ),
        migrations.AddField(
            model_name='entrega',
            name='Vehiculo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Vehiculo'),
        ),
    ]
