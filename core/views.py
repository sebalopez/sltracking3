    # -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse, reverse_lazy
from core.models import *
import datetime
import requests
from itertools import chain
import json
import os
from unidecode import unidecode
import base64
from openpyxl import load_workbook
# from Crypto.Cipher import AES
#import requests
import ast
import random 
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def login_user(request):
    template_name = "login.html"
    logout(request)
    username = password = ''
    if request.POST:
        user = authenticate(username=request.POST["Username"], password=request.POST["Password"])
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse_lazy('index'))
    return render(request,template_name,{})

@login_required(login_url='login')
def index(request):
	template_name = 'index.html'
	return render(request, template_name, {})

@login_required(login_url='login')
def vehiculos(request):
    template_name = 'vehiculos.html'
    listado_flota = {}
    Flota = Vehiculo.objects.all()
    listado_flota["Flota"] = Flota
    return render(request, template_name, listado_flota)

@login_required(login_url='login')
def flota(request):
    template_name = "flota.html"
    listado_flota = {}
    Flota = Vehiculo.objects.all()
    listado_flota["Flota"] = Flota
    return render(request,template_name, listado_flota)

@login_required(login_url='login')
def seguimientodepedidos(request):
    template_name = 'seguimientodepedidos.html'
    seguimiento_pedidos = {}
    Seguimiento = Entrega.objects.all()
    seguimiento_pedidos["Seguimiento"] = Seguimiento
    return render(request, template_name, seguimiento_pedidos)

@login_required(login_url='login')
def listadodeentregas(request):
    template_name = "listadodeentregas.html"
    listado_datos = {}
    Entregas = Entrega.objects.all()
    listado_datos["Entregas"] = Entregas
    return render(request,template_name, listado_datos)

@login_required(login_url='login')
def upload(request):
    data = {}
    if request.FILES:
        #try:
        excel = request.FILES['excel']
        fs_excel = FileSystemStorage()
        filename_excel = fs_excel.save(excel.name.lower().replace(" ", "_"),excel)
        print filename_excel
        excel_url = fs_excel.url(filename_excel)[1:]
        wb = load_workbook(str(excel_url))
        for x in wb.get_sheet_names():
            print x
            for y in wb[x].iter_rows():
                print y
                row = []
                for z in y:
                    print z
                    print z.value, type(z.value)
                    try:
                        row.append(unidecode(z.value))
                    except:
                        row.append(str(z.value))
                print row
                camion = get_object_or_404(Vehiculo, Patente= row[3])
                entregas1 = Entrega.objects.create(Nro_documento=row[0],
                                                   Tipo_documento=row[1],
                                                   Orden_compra=row[2],
                                                   Vehiculo=camion,
                                                   Transportista=row[4],
                                                   Destino=row[5],
                                                   Direccion_destino=row[6],
                                                   Lat_cliente=row[7],
                                                   Lng_cliente=row[8],
                                                   Cantidad_bultos=row[9],
                                                   Cantidad_pallets=row[10])
                entregas1.Status_entrega = "EN VIAJE"
                entregas1.save()
        return JsonResponse({
            "title":"Archivo cargado",
            "text": "Se cargó la planilla correctamente",
            "type": "success"
        })
        # except:
        #     return JsonResponse({
        #             "title":"Error",
        #             "text": "Ocurrió un error al guardar la planilla",
        #             "type": "error"
        #         })


    else:
        return JsonResponse({
            "title":"Seleccione un archivo!",
            "text": "Debe selccionar una archivo",
            "type": "warning"
        })

@login_required(login_url='login')
def maps(request):
    template_name = "maps.html"
    data_template = {}
    
    return render(request,template_name,data_template)

@login_required(login_url='login')
def get_points_trucks(request):
    Vehiculos_lista = []   
    Vehiculos_trans = Vehiculo.objects.all()
    for vehiculo in Vehiculos_trans:
        entregas_vehiculo = []
        lista_entregas = vehiculo.entrega_set.all()
        for entrega in lista_entregas:
            entregas_vehiculo.append({
                'destino':unicode(entrega.Destino),
                'latitude':entrega.Lat_cliente,
                'longitude':entrega.Lng_cliente,
            })
        Vehiculos_lista.append({
            'patente':unicode(vehiculo.Patente),
            'color':unicode(vehiculo.Color_vehiculo),
            'entregas':entregas_vehiculo
        })

    return JsonResponse({
        'detalles':Vehiculos_lista
    })

@login_required(login_url='login')
def asignaciondeviajes(request):
    if request.POST:
        print request.POST
        camion = get_object_or_404(Vehiculo, Patente=request.POST["vehiculo_transporte"])
        entrega = Entrega.objects.create(Vehiculo=camion,
            Transportista=request.POST["conductor_vehiculo"],
            Destino=request.POST["lugar_de_entrega"],
            Direccion_destino=request.POST["direccion_entrega"],
            Lat_cliente=request.POST["latitud_entrega"],
            Lng_cliente=request.POST["longitud_entrega"],
            Nro_documento=request.POST["numero_documento"],
            Orden_compra=request.POST["orden_compra_doc"],
            Tipo_documento=request.POST["tipo_doc"],
            Cantidad_bultos=request.POST["cantidad_bultos_qty"],
            Cantidad_pallets=request.POST["cantidad_pallets_qty"])
        entrega.Status_entrega = "EN VIAJE"
        entrega.save()
    patente_vehiculo = Vehiculo.objects.all()
    transportista_perfil = Perfiles_de_usuario.objects.all()
    template_name = "asignaciondeviajes.html"
    return render(request,template_name,{'Placa_patente' : patente_vehiculo, 'Vehiculo_transportista' : transportista_perfil})

@login_required(login_url='login')
def registrodedatos(request):
    template_name = "registrodedatos.html"
    registro_datos = {}
    Registros = Entrega.objects.all()
    registro_datos["Registros"] = Registros
    return render(request,template_name, registro_datos)

@login_required(login_url='login')
def rendiciondeviajes(request):
    template_name = "rendiciondeviajes.html"
    return render(request,template_name,{})

@login_required(login_url='login')
def viajes(request):
    template_name = "viajes.html"
    return render(request,template_name,{})

@login_required(login_url='login')
def viajes2(request):
    template_name = "viajes2.html"
    return render(request,template_name,{})

