from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from core import views as core_views

urlpatterns = [
	
	url(r'^$', core_views.index, name="index"),
	url(r'^login/$', core_views.login_user, name="login"),
	url(r'^maps/$', core_views.maps, name="maps"),
	url(r'^get_points_trucks/$', core_views.get_points_trucks, name="get_points_trucks"),
	url(r'^flota/$', core_views.flota, name="flota"),
	url(r'^vehiculos/$', core_views.vehiculos, name="vehiculos"),
	url(r'^seguimientodepedidos/$', core_views.seguimientodepedidos, name="seguimientodepedidos"),
	url(r'^listadodeentregas/$', core_views.listadodeentregas, name="listadodeentregas"),
	url(r'^asignaciondeviajes/$', core_views.asignaciondeviajes, name="asignaciondeviajes"),
	url(r'^registrodedatos/$', core_views.registrodedatos, name="registrodedatos"),
	url(r'^rendiciondeviajes/$', core_views.rendiciondeviajes, name="rendiciondeviajes"),
	url(r'^upload/$', core_views.upload, name="upload"),
	url(r'^viajes/$', core_views.viajes, name="viajes"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
