 $(function () {
          var map = {
               center: {lat: -33.4488897, lng: -70.6692655},
               zoom: 3,
              mapTypeId: 'roadmap'},

          map = new google.maps.Map(document.getElementById('map_canvas'), map),
              marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
              });

          var trafficLayer = new google.maps.TrafficLayer();
              trafficLayer.setMap(map);

          var input = document.getElementById('direccion_entrega');
          var autocomplete = new google.maps.places.Autocomplete(input, {types: ["geocode"]});

          autocomplete.bindTo('bounds', map);
          var infowindow = new google.maps.InfoWindow();
          google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
              infowindow.close();
              var place = autocomplete.getPlace();
              if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
              } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(16);
              }
              moveMarker(place.name, place.geometry.location);
              $('.MapLat').val(place.geometry.location.lat());
              $('.MapLon').val(place.geometry.location.lng());
          });
         
          function moveMarker(placeName, latlng) {
              marker.setPosition(latlng);
          }
      });
