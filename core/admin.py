# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from core.models import *
from django.contrib import admin

# Register your models here.
admin.site.register(Perfiles_de_usuario)
admin.site.register(Entrega)
admin.site.register(Vehiculo)
admin.site.register(Posicion)